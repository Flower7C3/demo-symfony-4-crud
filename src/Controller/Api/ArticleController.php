<?php

namespace App\Controller\Api;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 * @package App\Controller
 * @Route("/api/articles")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("", name="app_api_article_index", methods={"GET"})
     * @return Response
     */
    public function index(Request $request)
    {
        switch ($request->get('sortField')) {
            default:
            case 'date':
                $orderBy = 'date';
                break;
            case 'id':
                $orderBy = 'id';
                break;
            case 'title':
                $orderBy = 'title';
                break;
        }
        switch ($request->get('sortDirection')) {
            default:
            case 'desc':
                $orderDirection = 'desc';
                break;
            case 'asc':
                $orderDirection = 'asc';
                break;
        }
        $itemsPerPage = $request->get('items', 10);
        if ($itemsPerPage < 1) {
            $itemsPerPage = 10;
        }
        $page = $request->get('page', 1);
        if ($page < 1) {
            $page = 1;
        }
        $offset = $itemsPerPage * ($page - 1);
        $articles = $this->container->get('doctrine.orm.entity_manager')->getRepository('App:Article')->findBy([], [$orderBy => $orderDirection], $itemsPerPage, $offset);
        $response = [];
        /** @var Article $article */
        foreach ($articles as $article) {
            $response[] = [
                'id' => $article->getId(),
                'title' => $article->getTitle(),
                'author' => $article->getAuthor(),
                'date' => $article->getDate(),
            ];
        }
        return new JsonResponse($response);
    }

}
