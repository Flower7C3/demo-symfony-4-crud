<?php

namespace App\Controller\Core;

use App\DomainManager\Core\DefaultDomainManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class AbstractDefaultController extends AbstractController
{
    /** @var DefaultDomainManagerInterface */
    protected $domainManager;

    public function __construct(DefaultDomainManagerInterface $domainManager)
    {
        $this->domainManager = $domainManager;
    }

}
