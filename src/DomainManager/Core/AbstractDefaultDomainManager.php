<?php

namespace App\DomainManager\Core;

use App\Form\Handler\Core\DefaultFormHandlerInterface;
use App\Form\Handler\DefaultFormHandler;
use App\Repository\Core\DefaultEntityRepositoryInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractDefaultDomainManager implements DefaultDomainManagerInterface
{
    /** @var DefaultFormHandler */
    protected $formHandler;

    /** @var EntityManager */
    protected $entityManager;

    /** @var DefaultEntityRepositoryInterface */
    protected $entityRepository;

    /**
     * DomainManagerAbstract constructor.
     * @param DefaultFormHandlerInterface $formHandler
     * @param EntityManagerInterface $entityManager
     * @param string $entityName
     */
    public function __construct(DefaultFormHandlerInterface $formHandler, EntityManagerInterface $entityManager, $entityName)
    {
        $this->entityManager = $entityManager;
        $this->formHandler = $formHandler;
        $this->entityRepository = $this->entityManager->getRepository($entityName);
    }

}
