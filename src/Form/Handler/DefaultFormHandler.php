<?php

namespace App\Form\Handler;

use App\Entity\Core\DefaultEntityInterface;
use App\Form\Handler\Core\DefaultFormHandlerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class DefaultFormHandler implements DefaultFormHandlerInterface
{
    /** @var FormFactory */
    protected $_formFactory;

    /** @var EventDispatcherInterface */
    protected $_eventDispatcher;

    private $formName;

    public function __construct(FormFactoryInterface $formFactory, EventDispatcherInterface $eventDispatcher)
    {
        $this->_formFactory = $formFactory;
        $this->_eventDispatcher = $eventDispatcher;
    }

    /**
     * @param string $formName
     * @param DefaultEntityInterface $entity
     * @return FormInterface
     */
    public function createForm($formName, DefaultEntityInterface $entity): FormInterface
    {
        $this->formName = $formName;
        $form = $this->_formFactory->create($formName, $entity);
        $this->_eventDispatcher->dispatch('app.event.' . $this->formName . '.form_created');
        return $form;
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @param string $method
     * @return bool
     */
    public function handleRequest(FormInterface $form, Request $request, $method = Request::METHOD_POST): bool
    {
        if ($request->isMethod($method)) {
            $form->handleRequest($request);
            $this->_eventDispatcher->dispatch('app.event.' . $this->formName . '.form_handle_request');
            if ($form->isValid()) {
                $this->_eventDispatcher->dispatch('app.event.' . $this->formName . '.form_valid');
                return true;
            }
        }
        return false;
    }
}
