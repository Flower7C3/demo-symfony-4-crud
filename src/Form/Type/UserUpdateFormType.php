<?php

namespace App\Form\Type;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserUpdateFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->setMethod(Request::METHOD_PUT)
            ->add('name', TextType::class, [
                'label' => 'First name',
                'empty_data' => '',
            ])
            ->add('surname', TextType::class, [
                'label' => 'Last name',
                'empty_data' => '',
            ])
            ->add('phone', TextType::class, [
                'label' => 'Phone',
                'empty_data' => '',
            ])
            ->add('address', TextType::class, [
                'label' => 'Address',
                'empty_data' => '',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

}
